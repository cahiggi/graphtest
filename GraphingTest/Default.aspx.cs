﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GraphingTest
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGvData();

                BindChart();
            }
        }

        private void BindGvData()
        {
            gvData.DataSource = GetChartData();
            gvData.DataBind();
        }

        private void BindChart()
        {
            DataTable dsChartData = new DataTable();
            StringBuilder strScript = new StringBuilder();

            try
            {
                dsChartData = GetChartData();

                strScript.Append(@"<script type='text/javascript'>
                                    google.load('visualization', '1', {packages: ['corechart']});
                                   </script>

                                   <script type='text/javascript'>
                                    function drawVisualization() {
                                        var gdata = google.visualization.arrayToDataTable([['Day', 'TurbTemp_F'],");

                int i = 0;
                foreach (DataRow row in dsChartData.Rows)
                {
                    strScript.Append("['" + i.ToString() + "', " + row["TurbTemp_F"] + "],");
                    i++;
                }

                strScript.Remove(strScript.Length - 1, 1);
                strScript.Append("]);");

                strScript.Append("var options = { title: 'TurbTemp_F Example', vAxis: {title: 'Degrees Fahrenheit'}, hAxis: {title: 'Reading Number'}, seriesType: 'bars', series: {3: {type: 'area'}} };");
                strScript.Append("var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));");
                strScript.Append("chart.draw(gdata, options); }");
                strScript.Append("google.setOnLoadCallback(drawVisualization);");

                strScript.Append("</script>");

                ltScripts.Text = strScript.ToString();
            }
            catch
            {

            }
            finally
            {
                dsChartData.Dispose();
                strScript.Clear();
            }
        }

        private DataTable GetChartData()
        {
            DataSet dsData = new DataSet();

            try
            {
                SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["MainConnection"].ConnectionString);
                SqlCommand cmd = new SqlCommand("SELECT * FROM Station1", sqlCon);

                sqlCon.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    var tb = new DataTable();
                    tb.Load(dr);
                    dsData.Tables.Add(tb);
                }

                sqlCon.Close();
            }
            catch
            {
                throw;
            }

            return dsData.Tables[0];            
        }
    }
}