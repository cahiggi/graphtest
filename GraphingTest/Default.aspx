﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GraphingTest.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <h1>Google Chart API Example</h1>
    <form id="form1" runat="server">
    <div>
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>  
        <h2>Data from database</h2>
        <asp:GridView ID="gvData" runat="server">  
        </asp:GridView>  
        <br />  
        <br />  
        <h2>Graph Rendered with Above Data</h2>
        <asp:Literal ID="ltScripts" runat="server"></asp:Literal>  
        <div id="chart_div" style="width: 660px; height: 400px;">  
        </div>  
    </div>
    </form>
</body>
</html>
